#!/bin/sh

FILEMASK="reggioemilia-*.md"
DATE=`date`
TARGETDIR="psicologocasalecchio.gitlab.io"
CONFIGFILE="_config.yml"

mkdir ./pages/.tmp_pages
mv ./pages/$FILEMASK ./pages/.tmp_pages
bundle exec jekyll build --incremental --verbose --config _config.yml  -d ../$TARGETDIR/
cd ../psicologocasalecchio.gitlab.io
find ./ -name "*.htm*" | makesitemap.py https://psicologocasalecchio.com > sitemap.xml 
sed -i 's/index.html//g' sitemap.xml
git add .
git commit -m "Rilascio casalecchio $DATE"
git push
cd ../franci
mv ./pages/.tmp_pages/$FILEMASK ./pages
