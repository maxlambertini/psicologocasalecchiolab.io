#!/bin/sh

FILEMASK="bologna-*.md"
DATE=`date`
TARGETDIR="psicoloreggio.gitlab.io"
CONFIGFILE="_config.yml"

mkdir ./pages/.tmp_pages
mv ./pages/$FILEMASK ./pages/.tmp_pages
bundle exec jekyll build  --verbose --config _config.reggio.yml  -d ../$TARGETDIR/
cd ../$TARGETDIR
find ./ -name "*.htm*" | makesitemap.py https://psicologoreggio.com > sitemap.xml 
sed -i 's/index.html//g' sitemap.xml
git add .
git commit -m "Rilascio reggio $DATE"
git push
cd ../franci
mv ./pages/.tmp_pages/$FILEMASK ./pages
