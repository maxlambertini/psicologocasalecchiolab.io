<!--
<figure class="presentazione">
    <img id="img-francesca-de-lucca" src="/pics/presentazione.jpg" alt="Dottoressa Francesca De Lucca" />
</figure>
-->

E’ iscritta **dall’anno 2003** all’ Ordine degli Psicologi dell’ Emilia Romagna, e dal 2007 nella sezione degli  **Psicoterapeuti (sezione A, numero 3249)**, in seguito alla specializzazione in [Psicoterapia ad orientamento Psicoanalitico](/come-funziona/) conseguita presso l'Istituto di Analisi Immaginativa di Cremona. 

La Dottoressa si occupa di **[Psicoterapie individuali](/come-funziona/) e di [coppia con gli adulti](/mediazione-familiare/), in [età evolutiva](/eta-evolutiva/) di consulenza ai genitori, e [percorsi di sostegno psicologico o Psicoterapia con minori e adolescenti](/eta-evolutiva/)** e di **[Training Autogeno](/training-autogeno/)**

